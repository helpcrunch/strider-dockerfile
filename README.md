# Strider CD with Docker client

This image is latest stable version of [Strider CD](https://github.com/Strider-CD/strider) bundled with Docker client.
You will need a running container with [mongodb](https://registry.hub.docker.com/_/mongo/)

Example usage:

    docker run --name strider-mongo mongo

    docker run \
        -v /run/docker.sock:/run/docker.sock \
        --link strider-mongo:mongo \
        -e "DB_URI=mongodb://mongo/strider" \
        -p 3000:3000 \
        helpcrunch/strider
